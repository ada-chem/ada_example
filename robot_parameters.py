"""
A file for storing robot parameters. Each set of parameters will be unique to a given robot arm. The keyword arguments
are associated with the RobotArm class.
"""
# kwargs for Zippleback
arm_parameters = {
    'shoulder': {
        'cpr': 101000,
        'axis_range': [-300, 66600],
        'max_v': 65000,
        'max_a': 500000,
        'zero': 33148,
        'axis_symmetry': 1,
        'positive_rotation': 'CCW',
    },
    'elbow': {
        'cpr': 51000,
        'axis_range': [-150, 42150],
        'max_v': 65000,
        'max_a': 500000,
        'zero': 20974,
        'axis_symmetry': 1,
    },
    'gripper': {
        'cpr': 4000,  # counts per revolution
        'initial': 0,
        'max_rpm': 750,
        'axis_symmetry': 1, #Changed from 2 to 1 by BPM on 2018/08/23 to avoid 180 slide handler rotation issues
    },
    'z': {
        'cpr': 1000,
        'axis_range': [-120, 26300],
        'max_rpm': 6000,
        'cpmm': 100,  # counts per mm is the most relevant value here
        'zero': 31380,  # the value where the gripper would touch the bed (calculated and outside of range)
        'distsign': -1,
    },
    'kwargs': {
        'shax': 2,  # shoulder axis number
        'elax': 1,  # elbow axis number
        'grax': 0,  # gripper axis number
        'grop': 0,  # gripper output number (for engaging the gripper)
        'vertax': 3,  # vertical axis number
        'velocity': 40000,  # default velocity
        'acceleration': 150000,  # default acceleration
        'x_offset': 0.3,
        'y_offset': 0.197,
        'modules': [
            {  # Zippleback 2 mL HPLC gripper
                'el_point': 170.25,
                'length': 21.,  # apparently this is actually 21 mm, not 19
                'name': 'gripper',
                'module_type': 'ArmModule',
            },
            {  # combination pipette and needle probe gen 1
                'el_point': 212.5,
                'voffset': 14.,
                'length': 33.,
                'attachment_length': 12.,
                'top_width': 4.39,
                'bot_width': 3.87,
                'probe_base_offset': 27.,
                'name': 'probe',
                'module_type': 'DispenserProbe',
            },
        ]
    }
}
